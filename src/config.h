#ifndef __CONFIG_H
#define	__CONFIG_H


// 中位机存储的工步任务个数
#define	MAX_TASKS	10
// 中位机最大可控制下位机个数
#define	MAX_DEVS	24
// 下位机最大通道个数
#define	MAX_CHN		2



// 与下位机通信的can接口序号
#define CAN_IDX		0


// 心跳超时次数
#define HB_OMAX		5

#endif
