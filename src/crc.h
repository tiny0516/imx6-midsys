#ifndef	__CRC_H
#define	__CRC_H

#include "datatypes.h"

u16 getCrc16(u8* buf, s32 len);
u8 getCrc8(u8* buf,s32 len);

#endif
