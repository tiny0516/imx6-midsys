#include <IceUtil/IceUtil.h>
#include <Ice/Ice.h>
#include "ImxI.h"
#include "global.h"
#include "ImxErr.h"

using namespace std;
using namespace Imx;

bool 
DevInterfaceI::test(const Ice::Current&)
{
	std::cout << "test !!!!!!!!" << std::endl;
	return true;
}

void 
DevInterfaceI::rcvStep(const ::Imx::Step& ss, const Ice::Current&)
{
	cout << "###############"	<< endl;
	cout << (int)ss.workMode	<< endl;
	cout << (int)ss.loopStart	<< endl;
	cout << (int)ss.loopEnd		<< endl;
	cout << ss.loopCnt			<< endl;
	cout << ss.seq				<< endl;
	cout << ss.outputPower		<< endl;
	cout << ss.outputVoltage	<< endl;
	cout << ss.outputCurrent	<< endl;
	cout << ss.endVoltage		<< endl;
	cout << ss.endCurrent		<< endl;
	cout << ss.endCapcity		<< endl;
	cout << ss.endTime			<< endl;
	cout << "###############"	<< endl;
}

int
DevInterfaceI::rcvTask(const ::Imx::Task& task, const Ice::Current&)
{
	s32 ret;
	u32 size = task.steps.size();
	cout << "recvd works size [" << size << "]" << endl;
	cout << "recvd works name [" << task.name << "]" << endl;
	ret = gTaskManager.loadTask(task);
	if(ret == IE_OK)
	{
		cout << "task stored" << endl;
	}
	else
	{
		printf("err code ret : %d\n",ret);
	}
	return ret;
}

int
DevInterfaceI::taskControl(const u8 devId, const u8 chnNo, const u8 cmd, const Ice::Current&)
{
	int ret;
	//printf("dev[%x]  chn[%x]   cmd[%x]\n",devId,chnNo,cmd);
	switch(cmd)
	{
		case 0x02:	// 暂停任务
			ret = gTaskManager.pauseTask(devId, chnNo);
			break;
		case 0x03:	// 继续任务
			ret = gTaskManager.contTask(devId, chnNo);
			break;
		case 0x04:	// 停止任务
			ret = gTaskManager.stopTask(devId, chnNo);
			break;
		default:
			ret = IE_TASKCTRL_ERRCMD;
	}
	return ret;
}

// 通道查询
ChnStateReport
DevInterfaceI::queryChnState(const u8 devId, const u8 channelId, const Ice::Current&)
{
	return gDataManager.queryChnState(devId, channelId);
}

DevStateReport 
DevInterfaceI::queryDevState(const u8 devId, const Ice::Current&)
{
	return gDataManager.queryDevState(devId);
}

void
DevInterfaceI::setNotifyCallback(const Ice::Current&)
{
}
