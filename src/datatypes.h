#ifndef __DATATYPES_H
#define	__DATATYPES_H

typedef unsigned char	u8;
typedef	unsigned short	u16;
typedef unsigned int	u32;
typedef	unsigned long	u64;


typedef char	s8;
typedef	short	s16;
typedef	int		s32;
typedef	long	s64;


//===============================================================
//	中位机与下位机数据交流用数据结构
//===============================================================
typedef struct
{
	u8	chnNo;
	u8	workMode;
	u8	workState;
	u8	retCode;
	u32	voltage;
	u32	current;
	u32	temperature;
	u32	timestamp;
}ChnReport;		// 接收下位机数据报告用数据结构

typedef struct
{
	u32	dest;		//	boostTemp[31:16] chnID[15:8] deviceID[7:0]	
	u32	boostVoltage;
	//u32	boostTemp;
	u32	tempRange;	//	maxTemp[31:16] minTemp[15:0]
	u32	maxVoltage;
	u32	minVoltage;
	u32	maxCurrent;
	u32	minCurrent;
}TaskProtect;

#pragma pack(1)
typedef struct
{
	u8	workMode;	// 工作模式
	u32	seq;		// 工步序号
	u32	outputPower;
	u32	outputVoltage;
	u32	outputCurrent;
}StepNotify;	// 工部下发数据
#pragma pack()

typedef struct
{
	u32	seq;
	u32	endVoltage;
	u32	endCurrent;
	u32	endTime;
	u32	endCapcity;
	u32	endEnergy;
}FinishCondition;	// 
#endif
