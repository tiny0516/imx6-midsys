#include <Ice/Ice.h>
#include <stdlib.h>
#include "Imx.h"
#include "ImxI.h"
#include "ImxErr.h"

using namespace std;
using namespace Imx;

void print_csr(ChnStateReport csr)
{
	int i;
	//printf("chnCnt[%d] \n",csr.size());
	for(i=0;i<csr.size();i++)
	{
		printf("=======devId[%04d]============chnNo[%04d]==============\n",csr[i].devId,csr[i].chnNo);
		printf("  devId       => [%d]\n",csr[i].devId);
		printf("  chnNo       => [%d]\n",csr[i].chnNo);
		printf("  workMode    => [%d]\n",csr[i].workMode);
		printf("  chnVol      => [%d]\n",csr[i].chnVol);
		printf("=======================================================\n");
	}
}

void print_dsr(DevStateReport dsr)
{
	int i,j;
	ChnStateReport csr;
	for(i=0;i<dsr.size();i++)
	{
		csr = dsr[i].csr;
		print_csr(csr);
	}
}

int main(int argc, char* argv[])
{
		printf(">>>>>>1\n");
	int i,j,ret;
	string name;
	try
	{
		Ice::CommunicatorPtr ice;
		ice = Ice::initialize(argc,argv);
		Ice::ObjectPrx dev = ice->stringToProxy("DevInterfaceI:tcp -h 10.1.1.201 -p 10086");
		DevInterfacePrx devitf = DevInterfacePrx::checkedCast(dev);

		/*
		Ice::ObjectPrx soft = ice->stringToProxy("SoftInterfaceI:tcp -h 10.1.1.192 -p 10086");
		SoftInterfacePrx softp = SoftInterfacePrx::checkedCast(soft);
		if(softp == NULL)
		{
			printf("aaaaaaaaaaaaaaaaa\n");
		}

		string msg;
		msg = "msg test @ soft interface ";
		softp->msg(0x1b,msg);
		*/

		ret = -1;

		printf(">>>>>>2\n");
		for(i=0;i<1;i++)
		{
			Task task;
			task.devId = 0x11;
			task.chnNo = 0x2;
#if 1
			task.boostTemp = 0x1234;
			task.minTemp = 0x1122;
			task.maxTemp = 0x3344;
			task.boostVoltage = 0x12345678;
			task.minVoltage = 0x10012002;
			task.maxVoltage = 0x40045005;
			task.maxCurrent = 0x98765432;
			task.minCurrent	= 0x12345678;
#endif
			name = "test";
			name+= ('0'+i);
			task.name = name;
			task.steps.resize(1);

			for(j=0;j<1;j++)
			{
				task.steps[j].seq =j;
				task.steps[j].loopStart = 0;
				task.steps[j].loopEnd = 0;
				task.steps[j].loopCnt = 0;
#if 1
				task.steps[j].outputPower = 0x12345678;
				task.steps[j].outputVoltage = 0x99887766;;
				task.steps[j].outputCurrent = 0x55443322;
				task.steps[j].endVoltage = 0x11335577;
				task.steps[j].endCurrent = 0x22446688;
				task.steps[j].endCapcity = 0x13572468;
				task.steps[j].endEnergy = 0x13572468;
#endif
				task.steps[j].endTime = 0x40000010;

			}
			ret = devitf->rcvTask(task);
#if 0
				task.steps[1].loopStart = 1;
				task.steps[1].loopCnt = 3;
				task.steps[3].loopEnd = 1;

			sleep(7);
			ret = devitf->taskControl(0xa5,0x2,0x02);
			ret = devitf->taskControl(0xa5,0x2,0x03);
			ret = devitf->taskControl(0xa5,0x2,0x04);
#endif
			if(ret == IE_OK)
			{
				printf("work done well !\n");
			}
			else if(ret == IE_QUEUE_FULL)
			{
				printf("work full !\n");
			}
			printf("%d\n",ret);
		}
#if 0 // state test
		ChnStateReport csr;
		csr = devitf->queryChnState(1,1);
		print_csr(csr);
		csr = devitf->queryChnState(1,0);
		print_csr(csr);
		csr = devitf->queryChnState(2,4);
		print_csr(csr);
#endif
#if 1
		DevStateReport dsr;	
		dsr = devitf->queryDevState(0);
		print_dsr(dsr);
#endif
		if(ice)
			ice->destroy();
	}
	catch(ImxException& ex)
	{
		cerr << ex.reason<< endl;
	}
	return 0;
}
