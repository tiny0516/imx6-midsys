#include <Ice/Ice.h>
#include "Imx.h"
#include "ImxI.h"
#include "global.h"
#include "util.h"


using namespace std;
using namespace Imx;

// ICE服务初始化
int
initIceService(int argc, char *argv[])
{
	std::string endpoint = (const char*)"tcp -h";
	//endpoint += serverName;
	endpoint += "-p 10086";

	adapter = ice->createObjectAdapterWithEndpoints("ImxAdapter", "tcp -h 0.0.0.0 -p 10086");

	DevInterfacePtr devImpl = new DevInterfaceI();
	adapter->add(devImpl, ice->stringToIdentity("DevInterfaceI"));


	adapter->activate();
	return 0;
}

// 数据初始化
void
initDatas()
{
	gDataManager.initTestData();// 模拟数据
	can0.init();
}


// 工作线程启动
void
initWorks()
{
	pListen	= new Listen();
	pAnalysis = new Analysis();
	pNotify = new Notify();

	pNotify->start();
	printf(">>>Notify started !\n");

	pListen->start();
	printf(">>>Listen started !\n");

	pAnalysis->start();
	printf(">>>Analysis started !\n");
}

int 
main(int argc, char* argv[])
{
	try
	{
#if 1
		Ice::PropertiesPtr props = Ice::createProperties(argc,argv);
		props->setProperty("Ice.Default.Timeout", "3000");
		props->setProperty("Ice.Default.InvocationTimeout", "300000");
		props->setProperty("Ice.Default.Host", "localhost");
		props->setProperty("Ice.ThreadPool.Server.Size", "16");
		props->setProperty("Ice.ThreadPool.Server.SizeMax", "32");
		props->setProperty("Ice.ThreadPool.Client.Size", "16");
		props->setProperty("Ice.ThreadPool.Client.SizeMax", "16");
		props->setProperty("Ice.ThreadPool.Server.StackSize", "8192000");
		props->setProperty("Ice.MessageSizeMax","4096");
		
		Ice::InitializationData initData;
		
		initData.properties = props;
		ice = Ice::initialize(argc, argv,initData);
#endif
		//ice = Ice::initialize(argc, argv);

		initIceService(argc, argv);

		initDatas();
		initWorks();

		cout << ">>IMX Server Started !!!" << endl;
		ice->waitForShutdown();
	}
	catch(const exception &ex)
	{
		cerr << ex.what() << endl;
	}
	return 0;
}

