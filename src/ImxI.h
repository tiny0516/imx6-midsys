#ifndef	__IMX_H
#define	__IMX_H

#include "datatypes.h"
#include "Imx.h"

namespace Imx
{
	class DevInterfaceI : public DevInterface
	{
	public:
		virtual bool test(const Ice::Current&);
		virtual void rcvStep(const ::Imx::Step &, const Ice::Current&);
		virtual int rcvTask(const ::Imx::Task &, const Ice::Current&);
		virtual int taskControl(const u8, const u8, const u8, const Ice::Current&);
		virtual ChnStateReport queryChnState(const u8 , const u8, const Ice::Current&);
		virtual DevStateReport queryDevState(const u8, const Ice::Current&);
		virtual void setNotifyCallback(const Ice::Current&);

	};
}



#endif
