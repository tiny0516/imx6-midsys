#include <stdio.h>
#include <string.h>
#include "global.h"
#include "util.h"


using namespace Imx;


Notify::Notify():_stopped(false)
{
	_baseInterval = 1000;
	_hbCnt = 1;
	_dataCnt = 5;
}

struct timespec start_ts;

void 
Notify::run()
{
	s16 hb = _hbCnt;
	s16 data = _dataCnt;
	while(!_stopped)
	{

		gDataManager.hbCheck();
		if(hb == 0)
		{
			//printf("snd %ld\n",ts.tv_nsec);
			can0.detectNotify();
			hb = _hbCnt;
			//printf(">>>....detect notify\n");
		}

		if(data == 0)
		{
			clock_gettime(CLOCK_REALTIME, &start_ts);
			//can0.dataNotify();
			data = _dataCnt;
			//printf(">>>....data notify\n");
		}
		usleep(_baseInterval*1000);
		hb--;;
		data--;;
	}
}

