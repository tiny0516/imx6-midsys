// 全局变量定义
#ifndef	__GLOBAL_H
#define	__GLOBAL_H

#include <Ice/Ice.h>
#include "Imx.h"
#include "ImxErr.h"
#include "config.h"
#include "util.h"
#include "can.h"

namespace Imx
{
	// ice base
	extern Ice::CommunicatorPtr		ice;
	extern Ice::ObjectAdapterPtr	adapter;

	// threads
	extern IceUtil::ThreadPtr		pNotify;
	extern IceUtil::ThreadPtr		pTaskSchedule;
	extern IceUtil::ThreadPtr		pListen;
	extern IceUtil::ThreadPtr		pAnalysis;

	// management
	extern TaskManager				gTaskManager; //任务管理类
	extern DataManager				gDataManager; //数据管理类（保存设备通道状态信息）

	// global vars
	extern Can						can0;
	//extern queue<struct can_frame>	msgQueue;
}

#endif
