#ifndef	__IMXERR_H
#define	__IMXERR_H

#define	IE_OK				0

#define	IE_STEP_FINISH		1
#define	IE_END_REACH		10		//	达到截至条件
#define	IE_END_UNREACH		11		//	未达到截至条件

#define	IE_ACK_TIMEOUT		-10
#define	IE_QUEUE_FULL		-11
#define	IE_QUEUE_EMPTY		-12
#define	IE_ERR_PARAM		-20		//	参数错误
#define	IE_ERR_STEP			-21
#define	IE_ERR_SET			-22
#define	IE_ERR_DEVID		-23
#define	IE_TASKCTRL_ERRCMD	-31
#define	IE_TASKCTRL_NULLPTR	-32		//	no running task
#define	IE_TASKCTRL_ERRSET	-33		//	配置条件
#define	IE_DEV_OFFLINE		-41		

#endif
