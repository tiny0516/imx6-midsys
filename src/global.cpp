//	全局变量定义
#include <Imx.h>
#include "can.h"
#include "util.h"
#include "global.h"


namespace Imx
{
	Ice::CommunicatorPtr	ice;			// ice 对象
	Ice::ObjectAdapterPtr	adapter;		// ice 适配器对象

	IceUtil::ThreadPtr		pNotify;			// 通知送线程句柄
	IceUtil::ThreadPtr		pTaskSchedule;		// 工步调度线程句柄
	IceUtil::ThreadPtr		pListen;			// CAN消息接听线程
	IceUtil::ThreadPtr		pAnalysis;

	TaskManager				gTaskManager;		// 工步信息管理类

	DataManager				gDataManager;		// 数据管理类

	Can						can0;				// CAN设备实例
	//queue<struct can_frame>	msgQueue;			// 接收消息队列
}
