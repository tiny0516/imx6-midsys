#pragma once

module Imx
{
	exception ImxException
	{
		string reason;
	};

	struct RecCond
	{
		int		deltaSec;
		int		deltaCur;
		int		deltaVol;
		int		deltaAh;
		int		deltaW;
		int		deltaWh;
	};

	struct Step
	{
		byte	workMode;		//	工作模式  0x01恒流充电 0x02恒压充电 0x03恒流恒压充电 0x04恒流放电 0x05恒压放电 0x06恒功率放电 0x07静置 0x08循环
		byte	loopStart;		//	中位机控制
		byte	loopEnd;		//	中位机控制
		int		loopCnt;		//	中位机控制
		int		seq;			//	下发
		int		outputPower;	//	下发	脉冲模式下 存放脉冲个数
		int		outputVoltage;	//	下发	脉冲模式下 高16位存放开通时间  低16存放关断时间
		int		outputCurrent;	//	下发	
		int		endVoltage;		//	中位机	[31:30] 10:小于等于	01:大于等于
		int		endCurrent;		//	中位机	同上
		int		endTime;		//	中位机	同上
		int		endCapcity;		//	中位机	同上
		int		endEnergy;		//	中位机	同上
		RecCond	cond;
	};
	sequence<Step> StepSeq;

	struct Task
	{
		string	name;
		byte	devId;
		byte	chnNo;
		short	boostTemp;	//	辅助温度
		short	maxTemp;	//	最大温度
		short	minTemp;	//	最小温度
		int		boostVoltage;	//	辅助电压
		int		maxVoltage;	//	最大电压
		int		minVoltage;	//	最小电压
		int		maxCurrent;	//	最大电流
		int		minCurrent;	//	最小电流
		StepSeq	steps;
	};
	sequence<Task> TaskSeq;

	struct TaskThread
	{
		int		threadId;
		bool	running;
		string	threadname;
	};
	sequence<TaskThread> TaskThreadGroup;


	struct ChnState
	{
		bool	enReport;	// 报告使能
		byte	devId;		// 设备ID
		byte	chnNo;			// 通道ID
		byte	workMode;		// 工作模式  0x01恒流充电 0x02恒压充电 0x03恒流恒压充电 0x04恒流放电 0x05恒压放电 0x06恒功率放电 0x07静置 0x08循环
		string	taskName;		// 工步任务名称
		int		chnVol;			// 通道电压
		int		chnCur;			// 通道电流
		int		chnTemp;		// 通道温度
		int		chnState;		// 通道状态	[7:0] 0x01:充电 0x02:放电 0x03:静置 0x04:暂停 0x05:停止 0xff:错误	
		int		seq;			// 工步编号
		int		chnCap;			// 通道容量  Ah
		int		chnEng;			// 通道能量  Wh
		int		chnElapsed;		// 模块单工步耗时
		int		chnWorkElapsed; //通道工步总耗时
		//	以下属性作为通道终止记录供中位机使用
		int		endVoltage;		//	中位机	[31:30] 10:小于等于	01:大于等于
		int		endCurrent;		//	中位机	同上
		int		endCapcity;		//	中位机	同上
		int		endEnergy;		//	中位机	同上
		int		endTime;		//	中位机	
	};
	sequence<ChnState> ChnStateReport;

	struct DevState
	{
		byte	devId;
		byte	devState;
		short	errCode;
		byte	cnt;		// 状态超时次数
		ChnStateReport	csr;

	};
	sequence<DevState> DevStateReport; 

	interface DevInterface
	{
		bool test();
		void rcvStep(Step ss);
		int rcvTask(Task task);
		int taskControl(byte devId, byte chnNo, byte cmd);	// cmd type : 0x01开始(保留) 0x02暂停 0x03继续 0x04 停止 
		ChnStateReport queryChnState(byte deviceId, byte channelId);
		DevStateReport queryDevState(byte deviceId);
		//void setLogCallback(SoftInterface *pSoftItf);	// 上位机日志代理下发
	};


	interface SoftInterface
	{
		void msg(byte type, string message);
	};
};
